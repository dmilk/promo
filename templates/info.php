<?php

if (!$data) {
    $header = $body = '';
} else {
    $header = ': Выдано ' . $data[0]['res_all'] . ' из ' . PROMO_COUNT;
    foreach ($data as $value) {
        $body .= '<tr>';
        $body .= "<td>$value[id]</td>";
        $body .= "<td>$value[res]</td>";
        $body .= "<td>".($value[itog] ?? 0). " из $value[value]</td></tr>";
    }
}
$text = <<<HTML
    <div>
        <input type="button" value="Назад" onclick="window.location.href = '/';">    
    </div>
    <p></p>
    <div>
        <h1>Информация об акции$header</h1>
    </div>
    <p></p>
    <table border="1">
        <tr>
            <td>Пользователь</td>
            <td>Выполнено</td>
            <td>Текущий прогресс</td>
        </tr>
        $body
    </table>
HTML;
?>