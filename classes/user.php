<?php

class User {

    static function create($refId = 0) {
        if (!$refId || !is_numeric($refId)) {
            return "<SCRIPT>alert('Error create user');</SCRIPT>";
        }

        try {
            $query = "insert into users(referral_id) values ($refId) RETURNING id;";
            $result = pg_query($query); // or die('Query failed: ' .$query.''. pg_last_error());
            if (!$result) {
                return "<SCRIPT>alert('DB Error create user!');</SCRIPT>";
            }
            $result = pg_fetch_all($result, PGSQL_ASSOC) ?? [];
            Promo::getPromo($result[0]['id']);
            return "<SCRIPT>alert('User " . $result[0]['id'] . " create user.');</SCRIPT>";
        } catch (\Exception $e) {
            return "<SCRIPT>alert('DB Error create user.');</SCRIPT>";
        }
    }

    static function getUser($userId): array {
        $query = "SELECT * from users where id = $userId";
        $result = pg_query($query) or message_die('Query failed: ' . $query . pg_last_error());
        return pg_fetch_all($result, PGSQL_ASSOC)[0] ?? [];
    }

    static function getUsers(): array {
        $query = 'SELECT id from users order by id';
        $result = pg_query($query) or message_die('Query failed: ' . $query . pg_last_error());
        $result = pg_fetch_all($result, PGSQL_ASSOC);
        if ($result === false)
            return [];
        return $result;
    }

    static function getUsersOption() {
        $refferals = self::getUsers();
        $result = '';
        foreach ($refferals as $value) {
            $result .= '<option value="' . $value['id'] . '">' . $value['id'] . '</option>';
        }
        return $result;
    }

    static function getAllInfo() {
        $query = 'select user_id id, value, itog, res, res_all from
	(select user_id, value, itog
		,row_number()  over(PARTITION BY pre.user_id order by date_to desc) n
		, SUM(cond) over(PARTITION BY pre.user_id) res
		, SUM(cond) over() res_all
	from (
			select p.id, p.user_id, p.date_to, p.value
				, sum(amount) over(PARTITION BY p.id) itog
				, row_number()  over(PARTITION BY p.id) num
				, CASE WHEN  (sum(amount) over(PARTITION BY p.id)) >= p.value THEN 1 else 0 END as cond
			from (select promo.*, u.id child_id from promo left join users u on u.referral_id = promo.user_id) p
			left join purchase pc on p.child_id = pc.user_id and pc.date_create between p.date_from and p.date_to
			order by p.user_id, p.id
		) pre
	where pre.num=1
	order by user_id) finish
        where n = 1';
        $result = pg_query($query) or message_die('Query failed: ' . $query . pg_last_error());
        $result = pg_fetch_all($result, PGSQL_ASSOC);
        if ($result === false)
            return [];
        return $result;
    }

}

?>
