<?php

class Promo {

    //Получаем (если необходимо, то создаем) текущий промо пользователя
    static function getPromo($userId = 0) {
        if (!$userId || !is_numeric($userId)) {
            return false;
        }

        try {
            while (true) {
                //Проверяем наличие промо у пользователя
                if (($promo ?? false) === false) {
                    $query = "select now() date_now, * from promo where user_id = $userId order by date_to desc limit 1";
                    $result = pg_query($query); // or message_die('Query failed: ' .$query.' '. pg_last_error());;
                    $promo = pg_fetch_all($result, PGSQL_ASSOC);
                    if ($promo)
                        $promo = $promo[0];
                }
                if ($promo === false) {
                    //Если промо вообще нет, то проверяем наличие пользователя и создаем промо с началом даты регистрации
                    $user = User::getUser($userId);
                    if (empty($user)) {
                        return false;
                    }
                    $query = "insert into promo (user_id, date_from, date_to, value)
                        values ($userId, '$user[date_create]', '$user[date_create]'::timestamp without time zone + '" . PROMO_DURATION . "days'::interval, " . PROMO_COST . ")
                        RETURNING now() date_now, *";
                    $result = pg_query($query);
                    $promo = pg_fetch_all($result, PGSQL_ASSOC);
                    if ($promo === false) {
                        return false;
                    }
                    $promo = $promo[0];
                }
                //Если промо актуален, то возвращаем
                if (strtotime($promo['date_now']) <= strtotime($promo['date_to'])) {
                    return $promo;
                }
                //Если промо не актуален, то проверяем выполнение условий для получения периода создания следующего промо
                $query = "select (CASE WHEN  (sum(amount) over()) > p.value THEN 1 else 0 END) as cond
                          from (select promo.*, u.id child_id 
                                from promo
                                left join users u on u.referral_id = promo.user_id
                                where promo.id= $promo[id]
                                ) p
                          left join purchase pc on p.child_id = pc.user_id and pc.date_create between p.date_from and p.date_to
                          limit 1";
                $result = pg_query($query);
                $result = pg_fetch_all($result);
                if ($result === false) {
                    return false;
                }
                //Если условие выполнено, то начало периода от текущего окончания
                $datefrom = "'$promo[date_to]'::timestamp without time zone + interval '1 second'";
                //Если условие не выполнено, то начало периода раньше текущего окончания на дельту
                if (!$result[0]['cond']) {
                    $datefrom .= " - '" . PROMO_RETRO_DURATION . "days'::interval ";
                }
                $query = "insert into promo (user_id, date_from, date_to, value)
                          values ($userId, $datefrom, '$promo[date_to]'::timestamp without time zone + '" . PROMO_DURATION . "days'::interval, " . PROMO_COST . " )
                          RETURNING now() date_now, *";

                $result = pg_query($query);
                $promo = pg_fetch_all($result);
                if ($promo === false) {
                    return false;
                }
                $promo = $promo[0];
                //Если промо актуален, то возвращаем, если промо не актуален, то повторяем
                if (strtotime($promo['date_now']) <= strtotime($promo['date_to'])) {
                    return $promo;
                }
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    static function expand($userId = 0) {
        if (!$userId || !is_numeric($userId)) {
            return "<SCRIPT>alert('Error expand');</SCRIPT>";
        }
        $promo = self::getPromo($userId);
        $query = "update promo set date_to = date_to + '" . PROMO_ADD_DURATION . "days'::interval, value = value + " . PROMO_ADD_COST
                . " where id = $promo[id] returning date_to";
        $result = pg_query($query);
        $result = pg_fetch_all($result);
        if ($result) {
            $result = $result[0];
            return "<SCRIPT>alert('Акция продлена до $result[date_to] .');</SCRIPT>";
        }
        return "<SCRIPT>alert('Promo expand error create user.');</SCRIPT>";
    }

    static function createAll() {
        $query = 'select id from users u
                  left join (select user_id, max(date_to) date_to from promo group by user_id) p on p.user_id = u.id
                  where p.user_id is null or p.date_to < now()';
        $result = pg_query($query);
        while ($row = pg_fetch_row($result)) {
            self::getPromo($row[0]);
        }
        return true;
    }

}

?>
