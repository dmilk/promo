<?php

class Purchase {

    static function create($userId = 0, $amount = 0) {
        if (!$userId || !is_numeric($userId) || !$amount || !is_numeric($userId)) {
            return "<SCRIPT>alert('Error params');</SCRIPT>";
        }

        try {
            $query = "insert into purchase (user_id, amount) values ($userId, $amount)";
            $result = pg_query($query);
            if ($result !== false) {
                return "<SCRIPT>alert('Purchase create!');</SCRIPT>";
            }
        } catch (\Exception $e) {
            return "<SCRIPT>alert('DB Error create  purchase');</SCRIPT>";
        }
        return "<SCRIPT>alert('DB Error create purchase');</SCRIPT>";
    }

}

?>
