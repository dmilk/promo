<?php

require_once('controller_init.php');

if (($_REQUEST['route'] ?? '') === 'create' && $_SERVER['REQUEST_METHOD'] === 'POST' && ($_REQUEST['userId'] ?? 0) && ($_REQUEST['amount'] ?? 0)) {
    echo Purchase::create($_REQUEST['userId'] ?? 0, $_REQUEST['amount'] ?? 0);
}

if (($_REQUEST['route'] ?? '') === 'create') {
    $file = site_path . 'templates' . DS . 'purchase.php';
    $select = User::getUsersOption();
    include($file );
    echo $text;
}
?>
