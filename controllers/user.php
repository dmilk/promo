<?php

require_once('controller_init.php');

if (($_REQUEST['route'] ?? '') === 'create' && $_SERVER['REQUEST_METHOD'] === 'POST' && ($_REQUEST['refId'] ?? 0)) {
    echo User::create($_REQUEST['refId'] ?? 0);
}

if (($_REQUEST['route'] ?? '') === 'create') {
    $file = site_path . 'templates' . DS . 'user.php';
    $select = User::getUsersOption();
    include($file);
    echo $text;
}
?>
