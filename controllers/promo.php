<?php

require_once('controller_init.php');

if (($_REQUEST['route'] ?? '') === 'expand' && $_SERVER['REQUEST_METHOD'] === 'POST' && ($_REQUEST['userId'] ?? 0)) {
    echo Promo::expand($_REQUEST['userId']);
}

if (($_REQUEST['route'] ?? '') === 'expand') {
    $file = site_path . 'templates' . DS . 'promo.php';
    $select = User::getUsersOption();
    include($file );
    echo $text;
}
?>
