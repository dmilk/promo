<?php

define('PROMO_DURATION', 30);
define('PROMO_ADD_DURATION', 10);
define('PROMO_COST', 10000);
define('PROMO_ADD_COST', 3000);
define('PROMO_RETRO_DURATION', 10);
define('PROMO_COUNT', 307);

define('DS', DIRECTORY_SEPARATOR);
$site_path = realpath(dirname(__FILE__) . DS . '..' . DS) . DS;
define('site_path', $site_path);
require_once('../include/dbconnect.php');

spl_autoload_register(function ($class_name) {
    $filename = strtolower($class_name) . '.php';
    $file = site_path . 'classes' . DS . $filename;
    if (!file_exists($file)) {
        return false;
    }
    include ($file);
});
?>

