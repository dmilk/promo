--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 9.6.17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: promo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.promo (
    id integer NOT NULL,
    user_id integer,
    date_from timestamp without time zone DEFAULT now() NOT NULL,
    date_to timestamp without time zone DEFAULT (now() + '30 days'::interval) NOT NULL,
    value integer NOT NULL,
    CONSTRAINT promo_check CHECK ((date_from < date_to))
);


ALTER TABLE public.promo OWNER TO postgres;

--
-- Name: promo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.promo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.promo_id_seq OWNER TO postgres;

--
-- Name: promo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.promo_id_seq OWNED BY public.promo.id;


--
-- Name: purchase; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.purchase (
    id integer NOT NULL,
    user_id integer NOT NULL,
    amount numeric(15,2) NOT NULL,
    date_create timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.purchase OWNER TO postgres;

--
-- Name: purchase_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.purchase_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.purchase_id_seq OWNER TO postgres;

--
-- Name: purchase_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.purchase_id_seq OWNED BY public.purchase.id;


--
-- Name: purchase_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.purchase_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.purchase_user_id_seq OWNER TO postgres;

--
-- Name: purchase_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.purchase_user_id_seq OWNED BY public.purchase.user_id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    referral_id bigint DEFAULT 0,
    date_create timestamp without time zone DEFAULT now()
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: promo id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promo ALTER COLUMN id SET DEFAULT nextval('public.promo_id_seq'::regclass);


--
-- Name: purchase id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase ALTER COLUMN id SET DEFAULT nextval('public.purchase_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: promo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.promo VALUES (131, 11, '2020-02-17 12:00:15.909121', '2020-03-18 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (132, 11, '2020-03-08 12:00:16.909121', '2020-04-17 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (133, 11, '2020-04-07 12:00:16.909121', '2020-05-17 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (134, 11, '2020-05-07 12:00:16.909121', '2020-06-16 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (135, 11, '2020-06-06 12:00:16.909121', '2020-07-16 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (136, 11, '2020-07-06 12:00:16.909121', '2020-08-15 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (137, 11, '2020-08-05 12:00:16.909121', '2020-09-14 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (138, 11, '2020-09-04 12:00:16.909121', '2020-10-14 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (139, 11, '2020-10-04 12:00:16.909121', '2020-11-13 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (140, 11, '2020-11-03 12:00:16.909121', '2020-12-13 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (141, 11, '2020-12-03 12:00:16.909121', '2021-01-12 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (142, 11, '2021-01-02 12:00:16.909121', '2021-02-11 12:00:15.909121', 10000);
INSERT INTO public.promo VALUES (69, 3, '2021-02-17 12:20:12.478922', '2021-03-19 12:20:12.478922', 10000);
INSERT INTO public.promo VALUES (70, 4, '2021-02-17 12:20:26.540699', '2021-03-19 12:20:26.540699', 10000);
INSERT INTO public.promo VALUES (71, 5, '2021-02-17 12:23:21.348006', '2021-03-19 12:23:21.348006', 10000);
INSERT INTO public.promo VALUES (72, 7, '2021-02-18 11:53:54.157259', '2021-03-20 11:53:54.157259', 10000);
INSERT INTO public.promo VALUES (73, 8, '2021-02-18 11:54:02.978732', '2021-03-20 11:54:02.978732', 10000);
INSERT INTO public.promo VALUES (74, 9, '2021-02-18 11:54:40.453552', '2021-03-20 11:54:40.453552', 10000);
INSERT INTO public.promo VALUES (89, 14, '2021-02-18 12:33:37.000827', '2021-03-20 12:33:37.000827', 10000);
INSERT INTO public.promo VALUES (90, 15, '2021-02-18 12:34:13.22403', '2021-03-20 12:34:13.22403', 10000);
INSERT INTO public.promo VALUES (91, 16, '2021-02-18 15:47:52.499353', '2021-03-20 15:47:52.499353', 10000);
INSERT INTO public.promo VALUES (92, 17, '2021-02-18 15:48:32.987077', '2021-03-20 15:48:32.987077', 10000);
INSERT INTO public.promo VALUES (93, 18, '2021-02-18 15:50:50.136633', '2021-03-20 15:50:50.136633', 10000);
INSERT INTO public.promo VALUES (94, 19, '2021-02-19 07:07:16.230069', '2021-03-21 07:07:16.230069', 10000);
INSERT INTO public.promo VALUES (95, 20, '2021-02-19 07:07:51.271035', '2021-03-21 07:07:51.271035', 10000);
INSERT INTO public.promo VALUES (96, 21, '2021-02-19 07:07:58.717357', '2021-03-21 07:07:58.717357', 10000);
INSERT INTO public.promo VALUES (97, 22, '2021-02-19 07:09:06.509429', '2021-03-21 07:09:06.509429', 10000);
INSERT INTO public.promo VALUES (98, 23, '2021-02-19 07:09:59.137528', '2021-03-21 07:09:59.137528', 10000);
INSERT INTO public.promo VALUES (99, 24, '2021-02-19 07:10:49.625292', '2021-03-21 07:10:49.625292', 10000);
INSERT INTO public.promo VALUES (100, 25, '2021-02-19 07:49:30.452195', '2021-03-21 07:49:30.452195', 10000);
INSERT INTO public.promo VALUES (161, 12, '2020-02-18 12:00:51.175834', '2020-03-19 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (162, 12, '2020-03-09 12:00:52.175834', '2020-04-18 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (67, 1, '2021-02-17 08:41:13.313317', '2021-04-08 08:41:13.313317', 16000);
INSERT INTO public.promo VALUES (163, 12, '2020-04-08 12:00:52.175834', '2020-05-18 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (164, 12, '2020-05-08 12:00:52.175834', '2020-06-17 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (68, 2, '2021-02-17 11:17:01.951388', '2021-04-18 11:17:01.951388', 19000);
INSERT INTO public.promo VALUES (165, 12, '2020-06-07 12:00:52.175834', '2020-07-17 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (166, 12, '2020-07-07 12:00:52.175834', '2020-08-16 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (167, 12, '2020-08-06 12:00:52.175834', '2020-09-15 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (168, 12, '2020-09-05 12:00:52.175834', '2020-10-15 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (169, 12, '2020-10-05 12:00:52.175834', '2020-11-14 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (170, 12, '2020-11-04 12:00:52.175834', '2020-12-14 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (171, 12, '2020-12-04 12:00:52.175834', '2021-01-13 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (172, 12, '2021-01-03 12:00:52.175834', '2021-02-12 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (174, 26, '2021-02-19 07:49:34.932068', '2021-03-21 07:49:34.932068', 10000);
INSERT INTO public.promo VALUES (175, 12, '2021-02-02 12:00:52.175834', '2021-03-14 12:00:51.175834', 10000);
INSERT INTO public.promo VALUES (176, 27, '2021-02-19 16:54:58.12815', '2021-03-21 16:54:58.12815', 10000);
INSERT INTO public.promo VALUES (177, 28, '2021-02-19 16:55:40.021127', '2021-03-21 16:55:40.021127', 10000);
INSERT INTO public.promo VALUES (178, 29, '2021-02-19 16:56:19.694673', '2021-03-21 16:56:19.694673', 10000);
INSERT INTO public.promo VALUES (179, 30, '2021-02-20 11:52:34.772777', '2021-03-22 11:52:34.772777', 10000);
INSERT INTO public.promo VALUES (143, 11, '2021-02-01 12:00:16.909121', '2021-04-02 12:00:15.909121', 16000);


--
-- Name: promo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.promo_id_seq', 179, true);


--
-- Data for Name: purchase; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.purchase VALUES (1, 12, 33.11, '2021-02-18 12:23:42.694998');
INSERT INTO public.purchase VALUES (8, 12, 5479.45, '2021-02-19 13:15:15.763094');
INSERT INTO public.purchase VALUES (10, 19, 3000.00, '2021-02-19 21:20:16.3366');
INSERT INTO public.purchase VALUES (11, 19, 3000.00, '2021-02-19 21:20:23.887989');
INSERT INTO public.purchase VALUES (12, 19, 5000.00, '2021-02-19 21:20:31.282088');
INSERT INTO public.purchase VALUES (13, 19, 5000.00, '2021-01-19 21:47:51.680676');
INSERT INTO public.purchase VALUES (14, 23, 2111.00, '2021-02-19 21:48:28.192889');
INSERT INTO public.purchase VALUES (6, 24, 11.11, '2021-02-18 14:45:08.50001');
INSERT INTO public.purchase VALUES (7, 27, 11.00, '2021-02-18 14:45:20.474548');
INSERT INTO public.purchase VALUES (9, 12, 12000.00, '2020-02-19 13:15:15.763094');
INSERT INTO public.purchase VALUES (15, 11, 100.00, '2021-02-20 10:08:36.071272');
INSERT INTO public.purchase VALUES (16, 12, 100.00, '2021-02-20 10:11:25.979034');
INSERT INTO public.purchase VALUES (17, 12, 15000.00, '2020-11-01 10:25:45.412992');
INSERT INTO public.purchase VALUES (18, 12, 1500.00, '2021-02-20 11:52:45.307858');


--
-- Name: purchase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.purchase_id_seq', 18, true);


--
-- Name: purchase_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.purchase_user_id_seq', 1, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users VALUES (1, 0, '2021-02-17 08:41:13.313317');
INSERT INTO public.users VALUES (2, 1, '2021-02-17 11:17:01.951388');
INSERT INTO public.users VALUES (3, 1, '2021-02-17 12:20:12.478922');
INSERT INTO public.users VALUES (4, 1, '2021-02-17 12:20:26.540699');
INSERT INTO public.users VALUES (5, 1, '2021-02-17 12:23:21.348006');
INSERT INTO public.users VALUES (7, 1, '2021-02-18 11:53:54.157259');
INSERT INTO public.users VALUES (8, 1, '2021-02-18 11:54:02.978732');
INSERT INTO public.users VALUES (9, 1, '2021-02-18 11:54:40.453552');
INSERT INTO public.users VALUES (14, 8, '2021-02-18 12:33:37.000827');
INSERT INTO public.users VALUES (15, 2, '2021-02-18 12:34:13.22403');
INSERT INTO public.users VALUES (16, 2, '2021-02-18 15:47:52.499353');
INSERT INTO public.users VALUES (17, 1, '2021-02-18 15:48:32.987077');
INSERT INTO public.users VALUES (18, 1, '2021-02-18 15:50:50.136633');
INSERT INTO public.users VALUES (19, 12, '2021-02-19 07:07:16.230069');
INSERT INTO public.users VALUES (20, 1, '2021-02-19 07:07:51.271035');
INSERT INTO public.users VALUES (21, 1, '2021-02-19 07:07:58.717357');
INSERT INTO public.users VALUES (22, 1, '2021-02-19 07:09:06.509429');
INSERT INTO public.users VALUES (23, 12, '2021-02-19 07:09:59.137528');
INSERT INTO public.users VALUES (24, 12, '2021-02-19 07:10:49.625292');
INSERT INTO public.users VALUES (25, 1, '2021-02-19 07:49:30.452195');
INSERT INTO public.users VALUES (26, 1, '2021-02-19 07:49:34.932068');
INSERT INTO public.users VALUES (12, 11, '2020-02-18 12:00:51.175834');
INSERT INTO public.users VALUES (27, 12, '2021-02-19 16:54:58.12815');
INSERT INTO public.users VALUES (28, 1, '2021-02-19 16:55:40.021127');
INSERT INTO public.users VALUES (29, 5, '2021-02-19 16:56:19.694673');
INSERT INTO public.users VALUES (11, 2, '2020-02-17 12:00:15.909121');
INSERT INTO public.users VALUES (30, 11, '2021-02-20 11:52:34.772777');


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 30, true);


--
-- Name: promo promo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promo
    ADD CONSTRAINT promo_pkey PRIMARY KEY (id);


--
-- Name: purchase purchase_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase
    ADD CONSTRAINT purchase_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: promo promo_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.promo
    ADD CONSTRAINT promo_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE;


--
-- Name: purchase purchase_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchase
    ADD CONSTRAINT purchase_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE;


--
-- PostgreSQL database dump complete
--

